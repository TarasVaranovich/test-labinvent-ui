export class Comment {

  protected messageText: string;
  protected userName: string;
  protected parentID: number;

   constructor(parentID: number, messageText: string, userName: string) {

     this.parentID = parentID;
     this.messageText = messageText;
     this.userName = userName;

   }


  public getParentId():number {

    return this.parentID;

  }

  public getMessageText(): string {

    return this.messageText;

  }

  public getUserName(): string {

    return this.userName;

  }
}
