import { Comment } from './Comment';

export class CommentToDisplay extends Comment {

  private  isChoosen: boolean = false;
  public commentID: number;
  public annotatingUser: string;
  public annotationText: string;
  private uiOffset: number;

  constructor(commentID: number, parentID: number,messageText: string, userName: string) {

    super(parentID, messageText, userName);
    this.isChoosen = false;
    this.commentID = commentID;
    this.uiOffset = 0;
  }

  public setUIOffset(uiOffsetValue: number): void{

    this.uiOffset = uiOffsetValue;
  }

  public getUIOffset(): number {

    return this.uiOffset;
  }

  public isCommentChoosen(): boolean {

    return this.isChoosen;
  }

  public chooseComment(): void {

    this.isChoosen = true;
  }

  public unchooseComment(): void {

    this.isChoosen = false;
  }

  public static buildTreeUI(comments: CommentToDisplay[]):CommentToDisplay[]{

    let commentsUI: CommentToDisplay[] = [];
    commentsUI = comments;
    for(var i = 0; i < commentsUI.length; i++) {
      if(i > 0){

        if(commentsUI[i].getParentId() == commentsUI[i - 1].commentID){

          let newOffset = commentsUI[i].getUIOffset() + commentsUI[i - 1].getUIOffset() + 1;
          commentsUI[i].setUIOffset(newOffset);

        } else {//else if exists, if parent == 0, if parent !=0

            let commentParents = commentsUI.filter(comment => comment.commentID === commentsUI[i].getParentId());

            if(commentParents.length > 0) { // parent exists

              let newOffset = commentsUI[i].getUIOffset() + commentParents[0].getUIOffset() + 1;
              commentsUI[i].setUIOffset(newOffset);

            } else { // parent not exists

              if(commentsUI[i].getParentId() != 0){ // top comment

                commentsUI[i].setUIOffset(null); // free comment
              }
            }

        }

      } else { // first comment

        if(commentsUI[i].getParentId() != 0){

          commentsUI[i].setUIOffset(null);
        }

      }
    }
    return commentsUI;
  }
}
