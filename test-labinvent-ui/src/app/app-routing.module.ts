import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArticleComponent} from './article/article.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
  { path: 'article',     component: ArticleComponent},
  { path: 'search',     component: SearchComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
