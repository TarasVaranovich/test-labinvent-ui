import { Component } from '@angular/core';
//import { Router } from '@angular/router';
import { ApplicationService } from './application.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  template: `
  <h1>{{title}}</h1>
  <div>
    <nav>
      <a id="articleLink" routerLink="/article" routerLinkControl="active">article</a>
      <a id="userCommentsLink" routerLink="/search">search</a>
    </nav>
  </div>
  <div>
    <router-outlet></router-outlet>
  </div>
  <div style="display: block;
              position: fixed;
              width: 300px;
              height: 50px;
              top: 300px;
              left: 50px;
              background-color: yellow;
              border: 2px solid black;
              text-align: center;" *ngIf="applicationAlertMessage">
              {{ applicationAlertMessage }}
  </div>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Test for Labinvent';
  applicationAlertMessage = null;
  subscription: Subscription;

  constructor(private applicationService: ApplicationService){

    this.subscription = this.applicationService.getMessage().subscribe(applicationMessage => {

      this.applicationAlertMessage = applicationMessage;

    });
  }
}
