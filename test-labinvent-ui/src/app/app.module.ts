import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ArticleComponent } from './article/article.component';
import { SearchComponent } from './search/search.component';
/*for rest-api work*/
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient} from '@angular/common/http';
import { RestService } from './rest.service';
/*components communication*/
import { ApplicationService } from './application.service';

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [HttpClient, RestService, ApplicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
