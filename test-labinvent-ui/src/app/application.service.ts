import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ApplicationService {
  
    private subject = new Subject<any>();

    sendMessage(applicationMessage: string) {

        this.subject.next(applicationMessage);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
