import { Component, OnInit } from '@angular/core';
import { Comment } from '../Comment';
import { CommentToDisplay } from '../CommentToDisplay';
import { RestService } from '../rest.service';
import { ApplicationService } from '.././application.service';

@Component({
  selector: 'article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  private articleTitle: string;
  private articleContent: string;
  private displayedComments: CommentToDisplay[] = [];
  private choosenCommentId: number;
  private globalCommentUser: string;
  private globalCommentMessage: string;
  /*auxilary variables*/
  private userKey: string;
  private pageType: string;
  private pageSize: number;
  private pageNumber: number;
  private srecificPageNumber: number;
  private applicationAlertMessage: string = 'Please wait for server answer...';
  private previousBtnDisabled: boolean;
  private nextBtnDisabled: boolean;

  constructor(private communicationService: RestService,
              private applicationService: ApplicationService) {

  }
  ngOnInit() {
    this.articleTitle = "Lorem..."; //alert("Rsult" + result)
    this.articleContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    this.communicationService.getUserKey().then(result => this.getUserKey(result.toString())).catch(error => alert("Error occured:" + error));
    this.choosenCommentId = null;
    /*init setting*/
    this.pageType = "desktop";
    this.pageSize = 4; /*Page size in comments*/
    this.pageNumber = 1;
    this.previousBtnDisabled = false;
    this.nextBtnDisabled = false;
    this.applicationService.sendMessage(null);
  }

  private getUserKey(resultString: string):void {

    this.userKey = resultString;
    //this.communicationService.getPage(this.userKey, this.pageNumber, this.pageSize, this.pageType).then((result: CommentToDisplay[]) => this.getPageComments(result)).catch(error => alert("Error occured:" + error));
    this.getSpecificPage(this.pageNumber, 0);
  }

  private annotateComment(commentId: number): void {

    this.choosenCommentId = commentId;
  }

  private cancelAnnotation(): void {

    this.choosenCommentId = null;
  }

  private getPageComments (requestedPageNumber: number,
                            responceComments:CommentToDisplay[],
                            pageRoute: number):void {

    if(responceComments.length > 0) {

      this.displayedComments = responceComments;
      this.pageNumber = requestedPageNumber;
      this.displayedComments = CommentToDisplay.buildTreeUI(this.displayedComments);
      //check dispalyed comments change
      /*for(let comm of this.displayedComments){
        console.log(comm.commentID + ":" +
                    comm.getParentId() + ":" +
                    comm.getUIOffset());
      }*/
      //end check dispalyed comments change
      this.applicationService.sendMessage(null);

    } else {

      switch(pageRoute){

        case 1:
          this.nextBtnDisabled = true;
          break;

        case -1:
          this.previousBtnDisabled = true;
          break;

        default:
          break;
      }

      this.applicationService.sendMessage(null);

    }
  }

  private sendComment(commentId: number): void {
    var newComment: Comment;
    if(commentId > 0) {

      var currentComment = this.displayedComments.find(x => x.commentID == commentId);
      newComment = new Comment(currentComment.commentID, currentComment.annotationText, currentComment.annotatingUser);
      this.displayedComments.find(x => x.commentID == commentId).annotatingUser = "";
      this.displayedComments.find(x => x.commentID == commentId).annotationText= "";

    } else {

      newComment = new Comment(0, this.globalCommentMessage, this.globalCommentUser);
      this.globalCommentMessage = "";
      this.globalCommentUser = "";
    }

    this.communicationService.addComment(newComment, this.userKey, this.pageType).then(result => this.sendingCommentResult(result.toString())).catch(error => alert("Error occured:" + error));
    this.applicationService.sendMessage(this.applicationAlertMessage);
    this.choosenCommentId = null;
  }

  private sendingCommentResult(resultString: string): void {
    let resultMessage: string;
    if(resultString == "false") {
      //AAA!!
      //alert("Comment not added :(");
      resultMessage = "Comment not added :(";

    } else if(resultString == "true"){
      //AAA!!
      //alert("Comment added successfully.");
      resultMessage = "Comment added successfully.";
      this.getSpecificPage(this.pageNumber, 0);

    }
    //this.applicationService.sendMessage(null);
    setTimeout(()=>{
      this.applicationService.sendMessage(resultMessage);
      console.log("After timeout...");
    }, 500);
    setTimeout(()=>{
      this.applicationService.sendMessage(null);
      console.log("After timeout...");
    }, 3000);
  }

  private getPreviousPage(): void {
    let previousPageNumber: number;
    previousPageNumber = this.pageNumber - 1;
    this.getSpecificPage(previousPageNumber, -1);
    this.nextBtnDisabled = false;
  }

  private getNextPage(): void {
    let nextPageNumber: number;
    nextPageNumber = this.pageNumber + 1;
    this.getSpecificPage(nextPageNumber, 1);
    this.previousBtnDisabled = false;
  }

  private getSpecificPage(specificPageNumber: number, pageRoute: number): void {
    let resultSize: number;
    this.applicationService.sendMessage(this.applicationAlertMessage);
    this.communicationService.getPage(this.userKey, specificPageNumber, this.pageSize, this.pageType).then((result: CommentToDisplay[]) => this.getPageComments(specificPageNumber, result, pageRoute)).catch(error => alert("Error occured:" + error));
    console.log("Get page number" + specificPageNumber);
  }
}
