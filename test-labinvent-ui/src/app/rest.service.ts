import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Comment } from './Comment';
import { CommentToDisplay } from './CommentToDisplay';
import { RequestOptions } from '@angular/http';

@Injectable()
export class RestService {

    constructor(private http: HttpClient) {

    }
    //ARTICLE FORM
    public addComment<T>(comment: Comment, userUUID: string, pageType: string ): Promise<T> {

        const commentJSON = JSON.stringify({ userName: comment.getUserName(),
                                            messageText:comment.getMessageText(),
                                            parentID: comment.getParentId()});

        return this.http.post('http://localhost:8080/test-labinvent/getcomments.htm/addcomment.htm?userUUID=' +
                                            userUUID
																						+ '&pageType='
                                            + pageType, commentJSON.toString(), {responseType: 'text'}).toPromise().then(this.extractUserKey).catch(this.handleError);;
    }

    public getUserKey<T>(): Promise<T> {

        return this.http.post('http://localhost:8080//test-labinvent/index.htm', null, {responseType: 'text'}).toPromise().then(this.extractUserKey).catch(this.handleError);
    }

    public getPage<T>(userKey: string, pageNumber: number, pageSize: number, pageType: string): Promise<T> {

      return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getspecificpage.htm?userUUID=' +
  																					userKey +
  																					'&pagenumber=' +
  																					pageNumber +
  																					'&pageSize=' + pageSize +
  																					'&pageType=' + pageType, {responseType: 'text'}).toPromise().then(this.extractPage).catch(this.handleError);
    }
    //END ARTICLE FORM

    //SEARCH FORM
    public getUserComments<T>(userName: string,
                              pageSizeSearch: number,
                              pageNumberSearch: number,
                              commentUUID: string){
      return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getusercomments.htm?userName=' +
                            																			userName +
                            																			'&pageSizeSearch=' +
                            																			pageSizeSearch +
                            																			'&pageNumberSearch=' +
                                                                  pageNumberSearch +
                            																			'&commentUUID=' + commentUUID, {responseType: 'text'}).toPromise().then(this.extractPage).catch(this.handleError);
    }

    public getCommentsContainsText(predeterminedText: string,
                                  pageSizeSearch: number,
                                  pageNumberSearch: number,
                                  commentUUID: string){
      return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getcommentscontainstext.htm?predeterminedtext=' +
                                                                    predeterminedText +
                                                                    '&pageSizeSearch=' +
                                                                    pageSizeSearch +
                                                                    '&pageNumberSearch=' +
                                                                    pageNumberSearch +
                                                                    '&commentUUID=' + commentUUID, {responseType: 'text'}).toPromise().then(this.extractPage).catch(this.handleError);
    }

    public getDialogPage(pageSizeDialog: number,
                        pageNumberDialog: number,
                        commentUUIDDialog: string){
      return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getdialogpage.htm?pageSizeDialog=' +
                                                                      pageSizeDialog +
                                                                      '&pageNumberDialog=' +
                                                                      pageNumberDialog +
                                                                      '&commentUUIDDialog=' +
                                                                      commentUUIDDialog, {responseType: 'text'}).toPromise().then(this.extractPage).catch(this.handleError);
    }
    //END SEARCH FORM

    private extractUserKey(userKeyString: string) {

      return userKeyString;
    }

    private extractPage(pageResopnse: string): CommentToDisplay[]{
      let commentsArray = JSON.parse(pageResopnse);
      let responceComments: CommentToDisplay[] = [];
      for(let index in commentsArray) {
        let comment = new CommentToDisplay(commentsArray[index].id,
                                            commentsArray[index].parentid,
                                            commentsArray[index].messageText,
                                            commentsArray[index].userName);
          responceComments.push(comment);
      }
      return responceComments;
    }

    private handleError(error: any): Promise<any> {
        //alert('An error occurred' + error.error.message);
        return Promise.reject(error.message || error);
    }

}
