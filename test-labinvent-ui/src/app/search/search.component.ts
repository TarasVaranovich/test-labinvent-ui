import { Component, OnInit } from '@angular/core';
import { CommentToDisplay } from '../CommentToDisplay';
import { RestService } from '../rest.service';
import { ApplicationService } from '.././application.service';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private displayedComments: CommentToDisplay[] = [];
  private searchTitle: string;
  private searchType: string;
  private searchPredicate: string = "";
  private commentKey: string;
  private pageNumberSearch: number;
  private pageSizeSearch: number;

  private dialogID: number;
  private pageNumberDialog: number;
  private pageSizeDialog: number;
  private dialogComments: CommentToDisplay[] = [];
  private applicationAlertMessage: string = 'Please wait for server answer...';
  private previousBtnDisabled: boolean;
  private nextBtnDisabled: boolean;
  private previousBtnDialogDisabled: boolean;
  private nextBtnDialogDisabled: boolean;

  constructor(private communicationService: RestService,
              private applicationService: ApplicationService) {

  }

  ngOnInit() {
    this.searchTitle = "Search comments";
    this.searchType = "username";
    let commentOne = new CommentToDisplay(0,0,"Message One", "User One");
    this.displayedComments.push(commentOne);
    this.commentKey = "searchedComment";
    this.pageNumberSearch = 1;
    this.pageSizeSearch = 4;
    this.dialogID = null;
    //this.dialogID = 99;
    this.pageNumberDialog = 1;
    this.pageSizeDialog = 4;
    this.dialogComments.push(commentOne);
    this.applicationService.sendMessage(null);

    this.previousBtnDisabled = false;
    this.nextBtnDisabled = false;
    this.previousBtnDialogDisabled = false;
    this.nextBtnDialogDisabled =  false;
  }

  private searchComments(searchPredicate,pageNumberSearch): void{

    if(this.searchPredicate != "") {

      //alert(this.searchPredicate + ":" + this.searchType);
      switch (this.searchType){
        case "username": {
          this.communicationService.getUserComments(this.searchPredicate, this.pageSizeSearch, pageNumberSearch - 1,this.commentKey).then((result: CommentToDisplay[]) => this.getPageCommentsSearch(result, pageNumberSearch)).catch(error => alert("Error occured:" + error));
          this.applicationService.sendMessage(this.applicationAlertMessage);
          break;
        }
        case "commenttext": {
          this.communicationService.getCommentsContainsText(this.searchPredicate, this.pageSizeSearch, pageNumberSearch - 1,this.commentKey).then((result: CommentToDisplay[]) => this.getPageCommentsSearch(result, pageNumberSearch)).catch(error => alert("Error occured:" + error));
          this.applicationService.sendMessage(this.applicationAlertMessage);
          break;
        }
        default: {

          alert("Error!");
          break;
        }
      }

    } else {

      this.applicationService.sendMessage("Please input a required text.");
      setTimeout(()=>{
        this.applicationService.sendMessage(null);
        console.log("After timeout...");
      }, 2000);

    }

  }

  private getPageCommentsSearch (responceComments:CommentToDisplay[],
                                 pageNumberSearch:number):void {

    if(responceComments.length > 0) {

      this.displayedComments = responceComments;
      this.pageNumberSearch = pageNumberSearch;

    } else {

      if(this.pageNumberSearch < pageNumberSearch){

        this.nextBtnDisabled = true;

      } else if(this.pageNumberSearch > pageNumberSearch){

        this.previousBtnDisabled = true;

      } else {

        this.applicationService.sendMessage("Search doesn't produce result.");
      }
    }
    setTimeout(()=>{
      this.applicationService.sendMessage(null);
      console.log("After timeout...");
    }, 2000);
  }

  private getPreviousPageSearch(){
    let previousPageNumber = this.pageNumberSearch - 1;
    this.searchComments(this.searchPredicate, previousPageNumber);
    this.nextBtnDisabled = false;

  }

  private getNextPageSearch(){
    let nextPageNumber = this.pageNumberSearch + 1;
    this.searchComments(this.searchPredicate, nextPageNumber);
    this.previousBtnDisabled = false;
  }
  //DIALOG METHODS
  private openDialogBranch(commentID: number){
    this.dialogID = commentID;
    this.pageNumberDialog = 1;
    this.getDialogPage(this.pageNumberDialog, this.dialogID);
    this.nextBtnDialogDisabled = false;
    this.previousBtnDialogDisabled = false;
    //ELSE GET -1 PAGE NUMBER
    //this.pageNumberDialog = -1
    //this.getDialogPage(this.pageNumberDialog, this.dialogID);
  }

  private getDialogPage(pageNumberDialog: number, dialogID: number): void{

    this.communicationService.getDialogPage(this.pageSizeDialog, pageNumberDialog, this.commentKey + dialogID.toString()).then((result: CommentToDisplay[]) => this.getPageCommentsDialog(result, pageNumberDialog)).catch(error => alert("Error occured:" + error));
    this.applicationService.sendMessage(this.applicationAlertMessage);
  }

  private closeDialogBranch(){
    this.dialogID = null;
    this.dialogComments = [];
  }

  private getPreviousPageDialog(){

    let previousPageNumber = this.pageNumberDialog - 1;

    if(previousPageNumber == 0) {

      previousPageNumber = -1;
    }

    console.log("Getting dialog page:" + previousPageNumber);
    this.getDialogPage(previousPageNumber, this.dialogID);
    this.nextBtnDialogDisabled = false;
  }

  private getNextPageDialog(){

    let nextPageNumber = this.pageNumberDialog + 1;

    if(nextPageNumber == 0) {

      nextPageNumber = 1;
    }

    console.log("Getting dialog page:" + nextPageNumber);
    this.getDialogPage(nextPageNumber, this.dialogID);
    this.previousBtnDialogDisabled = false;
  }

  private getPageCommentsDialog (responseComments:CommentToDisplay[],
                                 pageNumberDialog:number):void {

    if(responseComments.length > 0) {

      this.dialogComments = responseComments;
      this.pageNumberDialog = pageNumberDialog;
      this.dialogComments = CommentToDisplay.buildTreeUI(this.dialogComments);
      //test
      /*for(let comm of this.dialogComments) {
        console.log("id-" + comm.commentID + "-pid-" + comm.getParentId() + "-off-" + comm.getUIOffset());
      }*/
      //end test
    } else {
      if(pageNumberDialog > 0){

        this.nextBtnDialogDisabled =  true;

      } else if(pageNumberDialog < 0) {

        this.previousBtnDialogDisabled = true;

      } else {

        alert("Search doesn't produce result.");
      }
      //this.dialogComments = [];
      //this.closeDialogBranch();
    }
    this.applicationService.sendMessage(null);
  }
  //END DIALOG METHODS
}
